<?php

namespace App\Http\Controllers\Api;

use App\Events\TodoCreatedEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\TodoRequest;
use App\Http\Resources\TodoResource;
use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TodoController extends Controller
{
  public function index(){

      $todos = Todo::where('user_id' , auth()->user()->id)->orderBy('created_at' , 'desc')->get();

      return TodoResource::collection($todos);
    }

    public function store(TodoRequest $request){

      $todo = Todo::create([
        'text' => $request->text,
        'done' => 0,
        'user_id' => Auth::user()->id
      ]);

      event(new TodoCreatedEvent(Auth::user()));

      return new TodoResource($todo);
    }

    public function delete($id){
      Todo::destroy($id);
      return 'success';
    }

    public function changeDoneStatus($id){
      $todo = Todo::find($id);
      if($todo->done == 1){
        $update = 0;
      }else{
        $update = 1;
      }

      $todo->update([
        'done' => $update
      ]);

      return new TodoResource($todo);
    }
}
